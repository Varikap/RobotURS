import os
from tabulate import tabulate
import Podaci.Rad_sa_podacima
from Entiteti.Model import Okvir
from Entiteti.Model import Robot

okvirFajl = os.path.join(os.path.dirname(__file__), "fajlovi", "okviri.txt")

def ucitavanje_okvira():
    """ Funkcija koja ucitava sve okvire i vraca listu istih """
    okviri = []
    with open(okvirFajl, "r") as f:
        for l in f.readlines():
            polja = l.strip().split("|")
            oznaka = polja[0]
            opis = polja[1]
            duzina = float(polja[2])
            sirina = float(polja[3])
            visina = float(polja[4])
            tezina = float(polja[5])
            tip_materijala = polja[6]
            oznaka_robota = polja[7]
            robot = deo_za_robota(Podaci.Rad_sa_podacima.ucitavanje_robota(), oznaka_robota)
            okviri.append(Okvir(oznaka, opis, duzina, sirina, visina, robot, tezina, tip_materijala))
    return okviri

def deo_za_robota(lista, oznaka):
    """ Funkcija koja ide kroz listu robota i proverava da li je njegova oznaka jednaka sa oznakom robota koja pripada okviru """
    for l in lista:
        if oznaka == l.oznaka:
            return l
        
def stampaj(tabela, headers = [['Oznaka', 'Opis', 'Duzina', 'Sirina', 'Visina', 'Tezina', 'Tip materijala']]):
    """ Funkcija koja stampa prosledjenu listu """
    print(tabulate(headers + tabela, headers='firstrow', tablefmt='grid'))
    
def vrednosti_okvira(okviri):
    """ Funkcija koja uzima vrednosti okvira i vraca ih u listi da bi ih pripremili za stampanje """
    tabela = []
    for u in okviri:
        oznaka = u.oznaka
        opis = u.opis
        duzina = u.duzina
        sirina = u.sirina
        visina = u.visina
        tezina = u.tezina
        tip_materijala = u.tip_materijala
        tabela.append([oznaka,opis,duzina, sirina, visina, tezina, tip_materijala])
    return tabela


def prikaz_okvira():
    """ Funkcija koja samo poziva stampanje """
    stampaj(vrednosti_okvira(ucitavanje_okvira()))
    
def prikaz_tezina_sortirano():
    """ Funkcija koja prikazuje sortirane okvire po tezini """
    stampaj(vrednost_recnik_okvira(sortiranje("tezina", ucitavanje_okvira_recnik())))
        
    
def upis_okvira(okviri):
    """ Funkcija koja upisuje prosledjene okvire u fajl """
    with open(okvirFajl,"a") as f:
        for r in okviri:
            f.write("{}|{}|{}|{}|{}|{}|{}|{}\n".format(r.oznaka,r.opis,r.duzina,r.sirina,r.visina,r.tezina,r.tip_materijala,r.robot))

def lista_za_upis_okvira():
    """ Funkcija koja zahteva od korisnika da unese novi okvir, koji posle apenduje u listu koju vraca"""
    oznake = ucitavanje_okvira()
    oznake_robota = Podaci.Rad_sa_podacima.ucitavanje_robota()
    okviri = []
    unos1 = input("Unesite oznaku okvira: ")
    for o in oznake:
        if unos1 == o.oznaka:
            print("okvir sa datom oznakom vec postoji")
            break
    else:
        unos2 = input("Unesite opis okvira: ")
        unos3 = input("Unesite duzinu okvira: ")
        unos4 = input("Unesite sirinu okvira: ")
        unos5 = input("Unesite visinu okvira: ")
        unos6 = input("Unesite tezinu okvira: ")
        unos8 = input("Unesite tip materijala okvira: ")
        unos7 = input("Unesite oznaku robota kojem okvir ide: ")
        try:
            for l in oznake_robota:
                if unos7 ==l.oznaka:
                    oznaka = unos1
                    opis = unos2
                    duzina = float(unos3)
                    sirina = float(unos4)
                    visina = float(unos5)
                    tezina = float(unos6)
                    robot = unos7
                    tip_materijala = unos8
                    okviri.append(Okvir(oznaka, opis, duzina, sirina, visina, robot, tezina, tip_materijala))
                    break
            else:
                print("nema robota sa tom oznakom!")
        except ValueError:
            print("niste uneli validne vrednosti")
    return okviri


def izmena_okvira(lista):
    """ Funkcija koja menja vec postojeci okvir. Proveri da li je unesena oznaka jednaka sa nekom u fajlu i trazi od korisnika da menja podatke koje on zeli """
    oznake_robota = Podaci.Rad_sa_podacima.ucitavanje_robota()
    unos1 = input("Unesite oznaku okvira: ")
    brojac = 0
    for d in lista:
        if d.oznaka == unos1:
            brojac +=1
            unos_odg="0"
            while unos_odg not in ["1","2",'3','4','5','6','7','8']:
                print("koju stvar zelite da izmenite? "
                      "\n1-opis"
                      "\n2-duzina"
                      "\n3-sirina"
                      "\n4-visina"
                      "\n5-tezina"
                      "\n6-tip materijala"
                      "\n7-kojem robotu pripada"
                      "\n8-sve")
                unos_odg=input("unesite broj za odgovarajucu opciju: ")
            
            if unos_odg =="1":
                unos2 = input("unesite zeljeni opis: ")
                d.opis = unos2
                return lista
                break
            elif unos_odg == "2":
                try:
                    unos3 = input("unesite novu duzinu: ")
                    d.duzina = float(unos3)
                except ValueError:
                    print("niste uneli pozitivan broj")
            elif unos_odg == "3":
                try:
                    unos4 = input("unesite novu sirinu: ")
                    d.sirina = float(unos4)
                except ValueError:
                    print("niste uneli pozitivan broj")  
            elif unos_odg == "4":
                try:
                    unos5 = input("unesite novu visinu: ")
                    d.visina = float(unos5)
                except ValueError:
                    print("niste uneli pozitivan broj")
            elif unos_odg == "5":
                try:
                    unos6 = input("unesite novu tezinu: ")
                    d.tezina = float(unos6)
                except ValueError:
                    print("niste uneli pozitivan broj")
            elif unos_odg == "6":
                unos7 = input("unesite novi tip materijala: ")
                d.tip_materijala = unos7
                return lista
                break
            elif unos_odg == "7":
                unos8 = input("unesite oznaku robota kojem dodeljujete: ")
                for k in oznake_robota:
                    if unos8 == k.oznaka:
                        d.robot = k
                return lista
                break
            elif unos_odg == "8":
                unos9 = input("unesite zejeni opis: ")
                unos10 = input("unesite zejenu duzinu: ")
                unos11 = input("unesite zejenu sirinu: ")
                unos12 = input("unesite zejenu visinu: ")
                unos13 = input("unesite zejenu tezinu: ")
                unos14 = input("unesite zejeni tip materijala: ")
                unos15 = input("unesite robota kojem pripada: ")
                try:
                    for k in oznake_robota:
                        if unos15 == k.oznaka:
                            d.robot =k
                            d.opis = unos9
                            d.duzina = float(unos10)
                            d.sirina = float(unos11)
                            d.visina = float(unos12)
                            d.tezina = float(unos13)
                            d.tip_materijala = unos14
                except ValueError:
                    print("niste uneli pozitivan broj")
    if brojac ==0:
        print("nema okvira sa tom oznakom")
        return lista
    return lista
        
        
def ucitavanje_okvira_recnik():
    """ Funkcija koja pravi recnik za okvirove atribute """
    okviri = []
    with open(okvirFajl, "r") as f:
        for l in f.readlines():
            o ={'oznaka' : l.split('|')[0],\
                'opis' : l.split('|')[1],\
                'duzina' : l.split('|')[2],\
                'sirina' : l.split('|')[3],\
                'visina' : l.split('|')[4],\
                'tezina' : l.split('|')[5],\
                'tip_materijala' : l.split('|')[6],\
                }

            okviri.append(o)
    return okviri

def vrednost_recnik_okvira(okviri):
    """ Funkcija koja uzima vrednosti iz recnika """
    tabela = []
    for u in okviri:
        oznaka = u["oznaka"]
        opis = u["opis"]
        duzina = u["duzina"]
        sirina = u["sirina"]
        visina = u["visina"]
        tezina = u["tezina"]
        tip_materijala = u["tip_materijala"]
        tabela.append([oznaka,opis,duzina, sirina, visina, tezina, tip_materijala])
    return tabela

def sortiranje(vrednost, lista):
    """ Funkcija koja prosledjenu listu po prosledjenom kljucu """
    return sorted(lista, key=lambda elem: "%s" % (elem[vrednost]))


def upis_izmene_okvira(okviri):
    """ Funkcija koja upisuje prosledjenu listu izmenjenih okvira u fajl """
    with open(okvirFajl,"w") as f:
        for r in okviri:
            f.write("{}|{}|{}|{}|{}|{}|{}|{}\n".format(r.oznaka,r.opis,r.duzina,r.sirina,r.visina,r.tezina,r.tip_materijala,r.robot.oznaka))
        
def prikaz_okvira_za_robota():
    """ Funkcija koja stampa samo one okvire sa oznakom koje je uneo korisnik"""
    roboti = Podaci.Rad_sa_podacima.ucitavanje_robota()
    unos = input("unesite oznaku okvira: ")
    rob = []
    brojac = 0
    with open(okvirFajl, "r") as f:
        for l in f.readlines():
            polja = l.strip().split("|")
            oznaka = polja[0]
            oznaka_robota = polja[7]
            if unos == oznaka:
                for r in roboti:
                    if r.oznaka == oznaka_robota:
                        rob.append(Robot(r.oznaka,r.opis,r.proizvodjac))
                        stampaj_robota(vrednosti_robota(rob))
                        brojac +=1
    if brojac == 0:
        print("nema okvira sa tom oznakom")

def stampaj_robota(tabela, headers = [['Oznaka', 'Opis', 'proizvodjac']]):
    print(tabulate(headers + tabela, headers='firstrow', tablefmt='grid'))
def stampaj_okvir(tabela, headers = [['Oznaka', 'Opis', 'duzina', 'sirina', 'visina', 'tezina', 'tip materijala']]):
    print(tabulate(headers + tabela, headers='firstrow', tablefmt='grid'))
    
def vrednosti_robota(rob):
    """ Funkcija koja uzima vrednosti robota """
    tabela = []
    for u in rob:
        oznaka = u.oznaka
        opis = u.opis
        proizvodjac = u.proizvodjac
        tabela.append([oznaka,opis,proizvodjac])
    return tabela
def pretrazi_okvir_oznaka():
    """ Funkcija koja trazi okvire za unetu korisnikovu oznaku """
    lista = ucitavanje_okvira()
    pretraga = []
    brojac = 0
    rec = input("unesite oznaku: ")
    for l in lista:
        if rec.lower() in l.oznaka.lower():
            pretraga.append(Okvir(l.oznaka, l.opis, l.duzina, l.sirina, l.visina, l.robot, l.tezina, l.tip_materijala))
            tabela = []
            for u in pretraga:
                oznaka = u.oznaka
                opis = u.opis
                duzina = u.duzina
                sirina = u.sirina
                visina = u.visina
                tezina = u.tezina
                tip_materijala = u.tip_materijala
                tabela.append([oznaka,opis,duzina,sirina,visina,tezina,tip_materijala])
    stampaj_okvir(tabela)
    brojac +=1
                
    if brojac ==0:
        print("ne postoji okvir sa tom oznakom")
        
def pretrazi_okvir_opis():
    """ Funkcija koja trazi okvire za unet korisnikov opis """
    lista = ucitavanje_okvira()
    pretraga = []
    brojac = 0
    rec = input("unesite opis: ")
    for l in lista:
        if rec.lower() in l.opis.lower():
            pretraga.append(Okvir(l.oznaka, l.opis, l.duzina, l.sirina, l.visina, l.robot, l.tezina, l.tip_materijala))
            tabela = []
            for u in pretraga:
                oznaka = u.oznaka
                opis = u.opis
                duzina = u.duzina
                sirina = u.sirina
                visina = u.visina
                tezina = u.tezina
                tip_materijala = u.tip_materijala
                tabela.append([oznaka,opis,duzina,sirina,visina,tezina,tip_materijala])
    stampaj_okvir(tabela)
    brojac +=1
                
    if brojac ==0:
        print("ne postoji okvir sa tim opisom")

def pretrazi_okvir_duzina():
    """ Funkcija koja trazi okvire za unetu korisnikovu duzinu"""
    lista = ucitavanje_okvira()
    pretraga = []
    brojac = 0
    tabela = []
    rec = input("unesite duzinu: ")
    try:
        
        for l in lista:
            if l.duzina >= float(rec):
                pretraga.append(Okvir(l.oznaka, l.opis, l.duzina, l.sirina, l.visina, l.robot, l.tezina, l.tip_materijala))
        for u in pretraga:
            oznaka = u.oznaka
            opis = u.opis
            duzina = u.duzina
            sirina = u.sirina
            visina = u.visina
            tezina = u.tezina
            tip_materijala = u.tip_materijala
            tabela.append([oznaka,opis,duzina,sirina,visina,tezina,tip_materijala])
            brojac +=1
        if brojac > 0:
            stampaj_okvir(tabela)      
        elif brojac ==0:
            print("ne postoji okvir sa tom duzinom")
    except ValueError:
        print("mora biti broj")

def pretrazi_okvir_sirina():
    """ Funkcija koja trazi okvire za unetu korisnikovu sirinu"""
    lista = ucitavanje_okvira()
    pretraga = []
    brojac = 0
    tabela = []
    rec = input("unesite sirinu: ")
    try:
        for l in lista:
            if l.sirina >= float(rec):
                pretraga.append(Okvir(l.oznaka, l.opis, l.duzina, l.sirina, l.visina, l.robot, l.tezina, l.tip_materijala))
        for u in pretraga:
            oznaka = u.oznaka
            opis = u.opis
            duzina = u.duzina
            sirina = u.sirina
            visina = u.visina
            tezina = u.tezina
            tip_materijala = u.tip_materijala
            tabela.append([oznaka,opis,duzina,sirina,visina,tezina,tip_materijala])
            brojac +=1
        if brojac > 0:
            stampaj_okvir(tabela)      
        elif brojac ==0:
            print("ne postoji okvir sa tom sirinom")
    except ValueError:
        print("mora biti broj")

def pretrazi_okvir_visina():
    """ Funkcija koja trazi okvire za unetu korisnikovu visinu """
    lista = ucitavanje_okvira()
    pretraga = []
    brojac = 0
    tabela = []
    rec = input("unesite visinu: ")
    try:
        for l in lista:
            if l.visina >= float(rec):
                pretraga.append(Okvir(l.oznaka, l.opis, l.duzina, l.sirina, l.visina, l.robot, l.tezina, l.tip_materijala))
        for u in pretraga:
            oznaka = u.oznaka
            opis = u.opis
            duzina = u.duzina
            sirina = u.sirina
            visina = u.visina
            tezina = u.tezina
            tip_materijala = u.tip_materijala
            tabela.append([oznaka,opis,duzina,sirina,visina,tezina,tip_materijala])
            brojac +=1
        if brojac > 0:
            stampaj_okvir(tabela)      
        elif brojac ==0:
            print("ne postoji okvir sa tom visinom")
    except ValueError:
        print("mora biti broj")

def pretrazi_okvir_tezina():
    """ Funkcija koja trazi okvire za unetu korisnikovu tezinu"""
    lista = ucitavanje_okvira()
    pretraga = []
    brojac = 0
    tabela = []
    rec = input("unesite tezinu: ")
    try:
        for l in lista:
            if l.tezina >= float(rec):
                pretraga.append(Okvir(l.oznaka, l.opis, l.duzina, l.sirina, l.visina, l.robot, l.tezina, l.tip_materijala))
        for u in pretraga:
            oznaka = u.oznaka
            opis = u.opis
            duzina = u.duzina
            sirina = u.sirina
            visina = u.visina
            tezina = u.tezina
            tip_materijala = u.tip_materijala
            tabela.append([oznaka,opis,duzina,sirina,visina,tezina,tip_materijala])
            brojac +=1
        if brojac > 0:
            stampaj_okvir(tabela)      
        elif brojac ==0:
            print("ne postoji okvir sa tom tezinom")
    except ValueError:
        print("mora biti broj")

def pretrazi_okvir_tip():
    """ Funkcija koja trazi okvire za unet korisnikov tip """
    lista = ucitavanje_okvira()
    pretraga = []
    brojac = 0
    rec = input("unesite tip materijala: ")
    for l in lista:
        if rec.lower() in l.tip_materijala.lower():
            pretraga.append(Okvir(l.oznaka, l.opis, l.duzina, l.sirina, l.visina, l.robot, l.tezina, l.tip_materijala))
            tabela = []
            for u in pretraga:
                oznaka = u.oznaka
                opis = u.opis
                duzina = u.duzina
                sirina = u.sirina
                visina = u.visina
                tezina = u.tezina
                tip_materijala = u.tip_materijala
                tabela.append([oznaka,opis,duzina,sirina,visina,tezina,tip_materijala])
    stampaj_okvir(tabela)
    brojac +=1
                
    if brojac ==0:
        print("ne postoji okvir sa tim tipom")

if __name__ == '__main__':
    pass