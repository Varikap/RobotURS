import os
from tabulate import tabulate

from Entiteti.Model import Robot

robotiFajl = os.path.join(os.path.dirname(__file__), "fajlovi", "roboti.txt")

def ucitavanje_robota():
    """ucitava fajl roboti.txt uzima podatke, pravi objekte
    robota i dodaje ih u listu"""
    roboti=[]
    with open(robotiFajl, "r") as g:
        for l in g.readlines():
            polje = l.strip().split("|")
            oznaka = polje[0]
            opis = polje[1]
            proizvodjac = polje[2]
            roboti.append(Robot(oznaka, opis, proizvodjac))
    return roboti

def upis_robota(roboti):
    """nasledjuje listu robota i upisuje ga u poslednji red"""
    with open(robotiFajl,"a") as f:
        for r in roboti:
            f.write("{}|{}|{}\n".format(r.oznaka,r.opis,r.proizvodjac))
            
def lista_za_upis_robota():
    """funkcija koja proverava da li robot sa datom oznakom vec postoji,
    pravi novog robota sa unetim podacima i prosledjuje listu za upis robota"""
    oznake = ucitavanje_robota()
    roboti = []
    unos1 = input("Unesite oznaku robota: ")
    for o in oznake:
        if unos1 == o.oznaka:
            print("robot sa datom oznakom vec postoji")
            break
    else:
        unos2 = input("Unesite opis robota: ")
        unos3 = input("Unesite proizvodjaca robota: ")
        oznaka = unos1
        opis = unos2
        proizvodjac = unos3
        roboti.append(Robot(oznaka, opis, proizvodjac))
    return roboti
def upis_izmene_robota(roboti):
    """funkcija koja uzima listu robota i upisuje ih u fajl roboti.txt"""
    with open(robotiFajl,"w") as f:
        for r in roboti:
            f.write("{}|{}|{}\n".format(r.oznaka,r.opis,r.proizvodjac))
            
def izmena_robota(lista):
    """funkcija koja daje opciju izmene odredjenih atributa robota
    i menja te atribute u listi"""
    roboti = lista
    brojac = 0
    unos1 = input("Unesite oznaku robota: ")
    for l in roboti:
        if l.oznaka == unos1:
            brojac +=1
            unos_odg="0"
            while unos_odg not in ["1","2",'3']:
                print("koju stvar zelite da izmenite? "
                      "\n1-opis"
                      "\n2-proizvodjaca"
                      "\n3-sve")
                unos_odg=input("unesite broj za odgovarajucu opciju: ")
            
            if unos_odg =="1":
                unos2 = input("unesite zeljeni opis: ")
                l.opis = unos2
                return roboti
                break
            elif unos_odg == "2":
                unos3 = input("unesite zeljenog proizodjaca: ")
                l.proizvodjac = unos3
                return roboti
                break
            elif unos_odg == "3":
                unos5 = input("uneiste zejeni opis: ")
                unos6 = input("uneiste zejenog proizvodjaca: ")
                l.opis = unos5
                l.proizvodjac = unos6
                return roboti
                break
    if brojac ==0:
        print("nema robota sa tom oznakom")
        return roboti
        

def stampaj(tabela, headers = [['Oznaka', 'Opis', 'Proizvodjac']]):
    """funkcija za stampanje tabele"""
    print(tabulate(headers + tabela, headers='firstrow', tablefmt='grid'))
    
def prikaz_robota():
    """funkcija za prikaz robot, stampa tabelu sa vrednostima robota"""
    stampaj(vrednosti_robota(ucitavanje_robota()))
    
def sortiranje(vrednost, lista):
    """funkcija za sortiranje liste"""
    return sorted(lista, key=lambda elem: "%s" % (elem[vrednost]))

def vrednosti_robota(roboti):
    """uzima vrednosti iz liste robota,
    koje se koriste za stampanje tabele"""
    tabela = []
    for u in roboti:
        oznaka = u.oznaka
        opis = u.opis
        proizvodjac = u.proizvodjac
        tabela.append([oznaka,opis,proizvodjac])
    return tabela

def pretrazi_robota_oznaka():
    """funkcija za pretrazivanje robota po oznaci,
    proverava da li je uneta rec u atributu oznaka,
     ako jeste dodaje ga u tabelu za stampanje"""
    lista = ucitavanje_robota()
    pretraga = []
    brojac = 0
    rec = input("unesite oznaku: ")
    for l in lista:
        if rec.lower() in l.oznaka.lower():
            pretraga.append(Robot(l.oznaka, l.opis, l.proizvodjac))
            tabela = []
            for u in pretraga:
                oznaka = u.oznaka
                opis = u.opis
                proizvodjac = u.proizvodjac
                tabela.append([oznaka,opis,proizvodjac])
    stampaj(tabela)
    brojac +=1
                
    if brojac ==0:
        print("ne postoji robot sa tom oznakom")
        
def pretrazi_robota_opis():
    """funkcija za pretrazivanje robota po opisu,
    proverava da li je uneta rec u atributu opis,
     ako jeste dodaje ga u tabelu za stampanje"""
    lista = ucitavanje_robota()
    pretraga = []
    brojac = 0
    rec = input("unesite opis: ")
    for l in lista:
        if rec.lower() in l.opis.lower():
            pretraga.append(Robot(l.oznaka, l.opis, l.proizvodjac))
            tabela = []
            for u in pretraga:
                oznaka = u.oznaka
                opis = u.opis
                proizvodjac = u.proizvodjac
                tabela.append([oznaka,opis,proizvodjac])
                brojac +=1
    if brojac ==0:
        print("ne postoji robot sa tim opisom")
    elif brojac > 0:
        stampaj(tabela)
        
def pretrazi_robota_proizvodjac():
    """funkcija za pretrazivanje robota po proizvodjacu,
    proverava da li je uneta rec u atributu proizvodjac,
     ako jeste dodaje ga u tabelu za stampanje"""
    lista = ucitavanje_robota()
    pretraga = []
    brojac = 0
    rec = input("unesite proizvodjaca: ")
    for l in lista:
        if rec.lower() in l.proizvodjac.lower():
            pretraga.append(Robot(l.oznaka, l.opis, l.proizvodjac))
            tabela = []
            for u in pretraga:
                oznaka = u.oznaka
                opis = u.opis
                proizvodjac = u.proizvodjac
                tabela.append([oznaka,opis,proizvodjac])
                brojac +=1
    if brojac ==0:
        print("ne postoji robot sa tim proizvodjacem")
    elif brojac >0:
        stampaj(tabela)
            

if __name__ == '__main__':
    pass

