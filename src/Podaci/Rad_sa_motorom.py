import os
from tabulate import tabulate
from Entiteti.Model import Motor
from Entiteti.Model import Robot
from Entiteti.Model import ElektricniDeo
#from Entiteti.Model import MehanickiDeo
import Podaci.Rad_sa_podacima
import Podaci.Rad_sa_okvirom
import Podaci.Rad_sa_senzorom

motorFajl = os.path.join(os.path.dirname(__file__), "fajlovi", "motori.txt")


def ucitavanje_motora():
    """ Funkcija koja ucitava motore iz fajla i smesta ih u listu koju vraca"""
    motori = []
    with open(motorFajl, "r") as f:
        for l in f.readlines():
            polja = l.strip().split("|")
            oznaka = polja[0]
            opis = polja[1]
            duzina = float(polja[2])
            sirina = float(polja[3])
            visina = float(polja[4])
            oznaka_robota = polja[5]
            tezina = float(polja[6])
            vreme_rada = int(polja[7])
            obrtaja_u_minuti = float(polja[8])
            potrosnja_po_obrtaju = float(polja[9])
            potrosnja = float(polja[10])
            robot = deo_za_robota(Podaci.Rad_sa_podacima.ucitavanje_robota(), oznaka_robota)
            motori.append(Motor(oznaka, opis, duzina, sirina, visina, robot, tezina, vreme_rada, obrtaja_u_minuti, potrosnja_po_obrtaju, potrosnja))
    return motori

def deo_za_robota(lista, oznaka):
    """ Funkcija koja ide kroz listu robota i proverava da li je njegova oznaka jednaka sa oznakom robota koja pripada motoru """
    for l in lista:
        if oznaka == l.oznaka:
            return l
        
def vrednosti_motora(motori):
    """ Funkcija koja uzima vrednosti motora i vraca ih u listi da bi ih pripremili za stampanje """
    tabela = []
    for u in motori:
        oznaka = u.oznaka
        opis = u.opis
        duzina = u.duzina
        sirina = u.sirina
        visina = u.visina
        tezina = u.tezina
        vreme_rada = u.vreme_rada
        obrtaja_u_minuti = u.obrtaja_u_minuti
        potrosnja_po_obrtaju = u.potrosnja_po_obrtaju
        potrosnja = u.potrosnja
        tabela.append([oznaka,opis,duzina,sirina,visina,tezina,vreme_rada,obrtaja_u_minuti,potrosnja_po_obrtaju,potrosnja])
    return tabela

def stampaj(tabela, headers = [['oznaka', 'opis', 'duzina', 'sirina', 'visina', 'tezina', 'vreme rada', 'obrtaja u minuti', 'potrosnja po obrtaju', 'potrosnja']]):
    print(tabulate(headers + tabela, headers='firstrow', tablefmt='grid'))

def prikaz_motora():
    """ Funkcija koja stampa motore. Poziva ostale """
    stampaj(vrednosti_motora(ucitavanje_motora()))

def lista_za_upis_motora():
    """ Funkcija koja dodaje novi motor u listu vec postojecih motora """
    oznake = ucitavanje_motora()
    oznake_robota = Podaci.Rad_sa_podacima.ucitavanje_robota()
    motori = []
    unos = input("Unesite oznaku motora: ")
    for o in oznake:
        if unos == o.oznaka:
            print("deo sa datom oznakom vec postoji")
            break
    else:
        unos2 = input("Unesite opis: ")
        unos3 = input("Unesite duzinu: ")
        unos4 = input("Unesite sirinu: ")
        unos5 = input("Unesite visinu: ")
        unos6 = input("Unesite tezinu: ")
        unos11 = input("Unesite vreme rada: ")
        unos12 = input("Unesite broj obrtaja u minuti: ")
        unos13 = input("Unesite potrosnju po obrtaju: ")
        unos14 = input("Unesite oznaku robota kojem motor ide: ")
        potrosnja =""
        try:
            for l in oznake_robota:
                if unos14 == l.oznaka:
                    oznaka = unos
                    opis = unos2
                    duzina = float(unos3)
                    sirina = float(unos4)
                    visina = float(unos5)
                    tezina = float(unos6)
                    vreme_rada = int(unos11)
                    obrtaja_u_minuti = float(unos12)
                    potrosnja_po_obrtaju = float(unos13)
                    robot = unos14
                    potrosnja = ElektricniDeo.elektricna_potrosnja(potrosnja, vreme_rada, obrtaja_u_minuti, potrosnja_po_obrtaju)
                    motori.append(Motor(oznaka,opis,duzina,sirina,visina,robot,tezina,vreme_rada,obrtaja_u_minuti,potrosnja_po_obrtaju,potrosnja))
                    break
            else:
                print("nema robota sa tom oznakom!")
        except ValueError:
            print("polja sa brojevima moraju imati broj")
    return motori

def upis_motora(motori):
    """ Funkcija koja prosledjenu listu motora upisuje u fajl """
    with open(motorFajl,"a") as f:
        for r in motori:
            f.write("{}|{}|{}|{}|{}|{}|{}|{}|{}|{}|{}\n".format(r.oznaka,r.opis,r.duzina,r.sirina,r.visina,r.robot,r.tezina,r.vreme_rada,r.obrtaja_u_minuti,r.potrosnja_po_obrtaju,r.potrosnja))

def vrednosti_robota(rob):
    """ Funkcija koja uzima vrednosti robota i smesta ih u listu koju vraca """
    tabela = []
    for u in rob:
        oznaka = u.oznaka
        opis = u.opis
        proizvodjac = u.proizvodjac
        tabela.append([oznaka,opis,proizvodjac])
    return tabela

def stampaj_robota(tabela, headers = [['Oznaka', 'Opis', 'proizvodjac']]):
    """ Funkcija koja  stampa robote"""
    print(tabulate(headers + tabela, headers='firstrow', tablefmt='grid'))
    
def prikaz_motora_za_robota():
    """ Funkcija koja  stampa robota za trazen motor"""
    roboti = Podaci.Rad_sa_podacima.ucitavanje_robota()
    unos = input("unesite oznaku motora: ")
    rob = []
    brojac = 0
    with open(motorFajl, "r") as f:
        for l in f.readlines():
            polja = l.strip().split("|")
            oznaka = polja[0]
            oznaka_robota = polja[5]
            if unos == oznaka:
                for r in roboti:
                    if r.oznaka == oznaka_robota:
                        rob.append(Robot(r.oznaka,r.opis,r.proizvodjac))
                        stampaj_robota(vrednosti_robota(rob))
                        brojac +=1
    if brojac == 0:
        print("motor sa takvom oznakom ne postoji")

def sortiranje(vrednost, lista):
    """ Funkcija koja  sortira primljenu listu po primljenom kljucu"""
    return sorted(lista, key=lambda elem: "%s" % (elem[vrednost]))

def prikaz_sortiranje_tezina():
    """ Funkcija koja  stampa motore sortirane po tezini"""
    stampaj(vrednost_recnik_motora(sortiranje("tezina",ucitavanje_motora_recnik())))
def prikaz_sortiranje_potrosnja():
    """ Funkcija koja  stampa motore sortirane po potrosnji"""
    stampaj(vrednost_recnik_motora(sortiranje("potrosnja",ucitavanje_motora_recnik())))
    
def ucitavanje_motora_recnik():
    """ Funkcija koja  ucitava motore iz fajla i smesta vrednosti u recnik"""
    motori = []
    with open(motorFajl, "r") as f:
        for l in f.readlines():
            o ={'oznaka' : l.split('|')[0],\
                'opis' : l.split('|')[1],\
                'duzina' : l.split('|')[2],\
                'sirina' : l.split('|')[3],\
                'visina' : l.split('|')[4],\
                'tezina' : l.split('|')[6],\
                'vreme_rada' : l.split('|')[7],\
                'obrtaja_u_minuti' : l.split('|')[8],\
                'potrosnja_po_obrtaju' : l.split('|')[9],\
                'potrosnja' : l.split('|')[10],\
                }

            motori.append(o)
    return motori

def vrednost_recnik_motora(motori):
    """ Funkcija koja  uzima vrednosti recnika i smesta ih u listu koju prosledjujemo za stampanje"""
    tabela = []
    for u in motori:
        oznaka = u["oznaka"]
        opis = u["opis"]
        duzina = u["duzina"]
        sirina = u["sirina"]
        visina = u["visina"]
        tezina = u["tezina"]
        vreme_rada = u["vreme_rada"]
        obrtaja_u_minuti = u["obrtaja_u_minuti"]
        potrosnja_po_obrtaju = u["potrosnja_po_obrtaju"]
        potrosnja = u["potrosnja"]
        tabela.append([oznaka,opis,duzina,sirina,visina,tezina,vreme_rada,obrtaja_u_minuti,potrosnja_po_obrtaju,potrosnja])
    return tabela
    
    
def izmena_motora(lista):
    """ Funkcija koja menja vrednosti za motore """
    oznake_robota = Podaci.Rad_sa_podacima.ucitavanje_robota()
    unos1 = input("Unesite oznaku motora: ")
    brojac = 0
    for d in lista:
        if d.oznaka == unos1:
            brojac +=1
            unos_odg="0"
            while unos_odg not in ["1","2",'3','4','5','6','7','8','9','10']:
                print("koju stvar zelite da izmenite? "
                        "\n1-opis"
                        "\n2-duzina"
                        "\n3-sirina"
                        "\n4-visina"
                        "\n5-tezinu"
                        "\n6-vreme rada"
                        "\n7-potrosnju po obrtaju"
                        "\n8-obrtaja u minuti"
                        "\n9-kojem robotu pripada"
                        "\n10-sve")
                unos_odg=input("unesite broj za odgovarajucu opciju: ")
            
            if unos_odg =="1":
                unos2 = input("unesite zeljeni opis: ")
                d.opis = unos2
                brojac +=1
            elif unos_odg == "2":
                unos3 = input("unesite novu duzinu: ")
                try:
                    d.duzina = float(unos3)
                    brojac +=1
                except ValueError:
                    print("morate uneti pravilno vrednosti")
            elif unos_odg == "3":
                unos4 = input("unesite novu sirinu: ")
                try:
                    d.sirina = float(unos4)
                    brojac +=1
                except ValueError:
                    print("morate uneti pravilno vrednosti")
            elif unos_odg == "4":
                unos5 = input("unesite novu visinu: ")
                try:
                    d.visina = float(unos5)
                    brojac +=1
                except ValueError:
                    print("morate uneti pravilno vrednosti")
            elif unos_odg =="5":
                unos6 = input("unesite tezinu: ")
                try:
                    d.tezina = float(unos6)
                    brojac +=1
                except ValueError:
                    print("morate uneti pravilno vrednosti")
            elif unos_odg == "6":
                unos7 = input("unesite vreme rada: ")
                try:
                    d.vreme_rada = int(unos7)
                    d.potrosnja = ElektricniDeo.elektricna_potrosnja(d.potrosnja, d.vreme_rada, d.obrtaja_u_minuti, d.potrosnja_po_obrtaju)
                    brojac +=1
                except ValueError:
                    print("morate uneti pravilno vrednosti")
            elif unos_odg == "7":
                unos8 = input("unesite potrosnju po obrtaju: ")
                try:
                    d.potrosnja_po_obrtaju = float(unos8)
                    d.potrosnja = ElektricniDeo.elektricna_potrosnja(d.potrosnja, d.vreme_rada, d.obrtaja_u_minuti, d.potrosnja_po_obrtaju)
                    brojac +=1
                except ValueError:
                    print("morate uneti pravilno vrednosti")
            elif unos_odg == "8":
                unos9 = input("unesite obrtaje u minuti: ")
                try:
                    d.obrtaja_u_minuti = float(unos9)
                    d.potrosnja = ElektricniDeo.elektricna_potrosnja(d.potrosnja, d.vreme_rada, d.obrtaja_u_minuti, d.potrosnja_po_obrtaju)
                    brojac +=1
                except ValueError:
                    print("morate uneti pravilno vrednosti")
            elif unos_odg == "9":
                unos10 = input("unesite oznaku robota kojem dodeljujete: ")
                for k in oznake_robota:
                    if unos10 == k.oznaka:
                        d.robot = k
                brojac +=1
            elif unos_odg == "10":
                unos11 = input("unesite zejeni opis: ")
                unos12 = input("unesite zejenu duzinu: ")
                unos13 = input("unesite zejenu sirinu: ")
                unos14 = input("unesite zejenu visinu: ")
                unos15 = input("unesite tezinu: ")
                unos16 = input("unesite vreme rada: ")
                unos17 = input("unesite obrtaje u minuti: ")
                unos18 = input("unesite potrosnju po obrtaju: ")
                unos19 = input("unesite robota kojem pripada: ")
                try:
                    for k in oznake_robota:
                        if unos19 == k.oznaka:
                            d.robot = k
                            d.opis = unos11
                            d.duzina = float(unos12)
                            d.sirina = float(unos13)
                            d.visina = float(unos14)
                            d.tezina = float(unos15)
                            d.vreme_rada = int(unos16)
                            d.obrtaja_u_minuti = float(unos17)
                            d.potrosnja_po_obrtaju = float(unos18)
                            d.potrosnja = ElektricniDeo.elektricna_potrosnja(d.potrosnja, d.vreme_rada, d.obrtaja_u_minuti, d.potrosnja_po_obrtaju)
                    brojac +=1
                except ValueError:
                    print("morate uneti pravilno vrednosti")
    if brojac ==0:
        print("nema motora sa tom oznakom")
    return lista
        
def upis_izmene_motora(motori):
    """ Funkcija koja upisuje vrednosti u fajl samo nakon izvrsene izmene motora """
    with open(motorFajl,"w") as f:
        for r in motori:
            f.write("{}|{}|{}|{}|{}|{}|{}|{}|{}|{}|{}\n".format(r.oznaka,r.opis,r.duzina,r.sirina,r.visina,r.robot.oznaka,r.tezina,r.vreme_rada,r.obrtaja_u_minuti,r.potrosnja_po_obrtaju,r.potrosnja))

def pretrazi_motor_oznaka():
    """ Funkcija koja  pretrazuje motor po oznaci"""
    lista = ucitavanje_motora()
    pretraga = []
    brojac = 0
    rec = input("unesite oznaku: ")
    for l in lista:
        if rec.lower() in l.oznaka.lower():
            pretraga.append(Motor(l.oznaka, l.opis, l.duzina, l.sirina, l.visina, l.robot, l.tezina, l.vreme_rada, l.obrtaja_u_minuti, l.potrosnja_po_obrtaju, l.potrosnja))
            tabela = []
            for u in pretraga:
                oznaka = u.oznaka
                opis = u.opis
                duzina = u.duzina
                sirina = u.sirina
                visina = u.visina
                tezina = u.tezina
                vreme_rada = u.vreme_rada
                obrtaja_u_minuti = u.obrtaja_u_minuti
                potrosnja_po_obrtaju = u.potrosnja_po_obrtaju
                potrosnja = u.potrosnja
                tabela.append([oznaka,opis,duzina,sirina,visina,tezina,vreme_rada,obrtaja_u_minuti,potrosnja_po_obrtaju,potrosnja])
                brojac +=1
    
                
    if brojac ==0:
        print("ne postoji motor sa tom oznakom")
    elif brojac >1:
        stampaj(tabela)

def pretrazi_motor_opis():
    """ Funkcija koja  pretrazuje motor po opisu"""
    lista = ucitavanje_motora()
    pretraga = []
    brojac = 0
    rec = input("unesite opis: ")
    for l in lista:
        if rec.lower() in l.opis.lower():
            pretraga.append(Motor(l.oznaka, l.opis, l.duzina, l.sirina, l.visina, l.robot, l.tezina, l.vreme_rada, l.obrtaja_u_minuti, l.potrosnja_po_obrtaju, l.potrosnja))
            tabela = []
            for u in pretraga:
                oznaka = u.oznaka
                opis = u.opis
                duzina = u.duzina
                sirina = u.sirina
                visina = u.visina
                tezina = u.tezina
                vreme_rada = u.vreme_rada
                obrtaja_u_minuti = u.obrtaja_u_minuti
                potrosnja_po_obrtaju = u.potrosnja_po_obrtaju
                potrosnja = u.potrosnja
                tabela.append([oznaka,opis,duzina,sirina,visina,tezina,vreme_rada,obrtaja_u_minuti,potrosnja_po_obrtaju,potrosnja])
                brojac +=1
    
                
    if brojac ==0:
        print("ne postoji motor sa tim opisom")
    elif brojac >1:
        stampaj(tabela)

def pretrazi_motor_duzina():
    """ Funkcija koja  pretrazuje motor po duzini"""
    lista = ucitavanje_motora()
    pretraga = []
    brojac = 0
    tabela = []
    rec = input("unesite duzinu: ")
    try:
        for l in lista:
            if l.duzina >= float(rec):
                pretraga.append(Motor(l.oznaka, l.opis, l.duzina, l.sirina, l.visina, l.robot, l.tezina, l.vreme_rada, l.obrtaja_u_minuti, l.potrosnja_po_obrtaju, l.potrosnja))

        for u in pretraga:
            oznaka = u.oznaka
            opis = u.opis
            duzina = u.duzina
            sirina = u.sirina
            visina = u.visina
            tezina = u.tezina
            vreme_rada = u.vreme_rada
            obrtaja_u_minuti = u.obrtaja_u_minuti
            potrosnja_po_obrtaju = u.potrosnja_po_obrtaju
            potrosnja = u.potrosnja
            tabela.append([oznaka,opis,duzina,sirina,visina,tezina,vreme_rada,obrtaja_u_minuti,potrosnja_po_obrtaju,potrosnja])
            brojac +=1
        if brojac > 0:
            stampaj(tabela)      
        elif brojac ==0:
            print("ne postoji motor sa tom duzinom")
    except ValueError:
        print("mora biti broj")

def pretrazi_motor_sirina():
    """ Funkcija koja  pretrazuje motor po sirini"""
    lista = ucitavanje_motora()
    pretraga = []
    brojac = 0
    tabela = []
    rec = input("unesite sirinu: ")
    try:
        for l in lista:
            if l.sirina >= float(rec):
                pretraga.append(Motor(l.oznaka, l.opis, l.duzina, l.sirina, l.visina, l.robot, l.tezina, l.vreme_rada, l.obrtaja_u_minuti, l.potrosnja_po_obrtaju, l.potrosnja))

        for u in pretraga:
            oznaka = u.oznaka
            opis = u.opis
            duzina = u.duzina
            sirina = u.sirina
            visina = u.visina
            tezina = u.tezina
            vreme_rada = u.vreme_rada
            obrtaja_u_minuti = u.obrtaja_u_minuti
            potrosnja_po_obrtaju = u.potrosnja_po_obrtaju
            potrosnja = u.potrosnja
            tabela.append([oznaka,opis,duzina,sirina,visina,tezina,vreme_rada,obrtaja_u_minuti,potrosnja_po_obrtaju,potrosnja])
            brojac +=1
        if brojac > 0:
            stampaj(tabela)      
        elif brojac ==0:
            print("ne postoji motor sa tom sirinom")
    except ValueError:
        print("mora biti broj")

def pretrazi_motor_visina():
    """ Funkcija koja  pretrazuje motor po visini"""
    lista = ucitavanje_motora()
    pretraga = []
    brojac = 0
    tabela = []
    rec = input("unesite visina: ")
    try:
        for l in lista:
            if l.visina >= float(rec):
                pretraga.append(Motor(l.oznaka, l.opis, l.duzina, l.sirina, l.visina, l.robot, l.tezina, l.vreme_rada, l.obrtaja_u_minuti, l.potrosnja_po_obrtaju, l.potrosnja))

        for u in pretraga:
            oznaka = u.oznaka
            opis = u.opis
            duzina = u.duzina
            sirina = u.sirina
            visina = u.visina
            tezina = u.tezina
            vreme_rada = u.vreme_rada
            obrtaja_u_minuti = u.obrtaja_u_minuti
            potrosnja_po_obrtaju = u.potrosnja_po_obrtaju
            potrosnja = u.potrosnja
            tabela.append([oznaka,opis,duzina,sirina,visina,tezina,vreme_rada,obrtaja_u_minuti,potrosnja_po_obrtaju,potrosnja])
            brojac +=1
        if brojac > 0:
            stampaj(tabela)      
        elif brojac ==0:
            print("ne postoji motor sa tom visinom")
    except ValueError:
        print("mora biti broj")

def pretrazi_motor_tezina():
    """ Funkcija koja  pretrazuje motor po tezini"""
    lista = ucitavanje_motora()
    pretraga = []
    brojac = 0
    tabela = []
    rec = input("unesite tezinu: ")
    try:
        for l in lista:
            if l.tezina >= float(rec):
                pretraga.append(Motor(l.oznaka, l.opis, l.duzina, l.sirina, l.visina, l.robot, l.tezina, l.vreme_rada, l.obrtaja_u_minuti, l.potrosnja_po_obrtaju, l.potrosnja))

        for u in pretraga:
            oznaka = u.oznaka
            opis = u.opis
            duzina = u.duzina
            sirina = u.sirina
            visina = u.visina
            tezina = u.tezina
            vreme_rada = u.vreme_rada
            obrtaja_u_minuti = u.obrtaja_u_minuti
            potrosnja_po_obrtaju = u.potrosnja_po_obrtaju
            potrosnja = u.potrosnja
            tabela.append([oznaka,opis,duzina,sirina,visina,tezina,vreme_rada,obrtaja_u_minuti,potrosnja_po_obrtaju,potrosnja])
            brojac +=1
        if brojac > 0:
            stampaj(tabela)      
        elif brojac ==0:
            print("ne postoji motor sa tom tezinom")
    except ValueError:
        print("mora biti broj")

def pretrazi_motor_obrtaja():
    """ Funkcija koja trazi motore po unetom broju obrtaja """
    lista = ucitavanje_motora()
    pretraga = []
    brojac = 0
    tabela = []
    rec = input("unesite obrtaje u minuti: ")
    try:
        for l in lista:
            if l.obrtaji_u_minuti >= float(rec):
                pretraga.append(Motor(l.oznaka, l.opis, l.duzina, l.sirina, l.visina, l.robot, l.tezina, l.vreme_rada, l.obrtaja_u_minuti, l.potrosnja_po_obrtaju, l.potrosnja))

        for u in pretraga:
            oznaka = u.oznaka
            opis = u.opis
            duzina = u.duzina
            sirina = u.sirina
            visina = u.visina
            tezina = u.tezina
            vreme_rada = u.vreme_rada
            obrtaja_u_minuti = u.obrtaja_u_minuti
            potrosnja_po_obrtaju = u.potrosnja_po_obrtaju
            potrosnja = u.potrosnja
            tabela.append([oznaka,opis,duzina,sirina,visina,tezina,vreme_rada,obrtaja_u_minuti,potrosnja_po_obrtaju,potrosnja])
            brojac +=1
        if brojac > 0:
            stampaj(tabela)      
        elif brojac ==0:
            print("ne postoji motor sa toliko obrtaja u minuti")
    except ValueError:
        print("mora biti broj")

def pretrazi_motor_potrosnja():
    """ Funkcija koja trazi motore po potrosnji"""
    lista = ucitavanje_motora()
    pretraga = []
    brojac = 0
    tabela = []
    rec = input("unesite potrosnju po obrtaju: ")
    try:
        for l in lista:
            if l.potrosnja_po_obrtaju >= float(rec):
                pretraga.append(Motor(l.oznaka, l.opis, l.duzina, l.sirina, l.visina, l.robot, l.tezina, l.vreme_rada, l.obrtaja_u_minuti, l.potrosnja_po_obrtaju, l.potrosnja))

        for u in pretraga:
            oznaka = u.oznaka
            opis = u.opis
            duzina = u.duzina
            sirina = u.sirina
            visina = u.visina
            tezina = u.tezina
            vreme_rada = u.vreme_rada
            obrtaja_u_minuti = u.obrtaja_u_minuti
            potrosnja_po_obrtaju = u.potrosnja_po_obrtaju
            potrosnja = u.potrosnja
            tabela.append([oznaka,opis,duzina,sirina,visina,tezina,vreme_rada,obrtaja_u_minuti,potrosnja_po_obrtaju,potrosnja])
            brojac +=1
        if brojac > 0:
            stampaj(tabela)      
        elif brojac ==0:
            print("ne postoji motor sa tolikom potrosnjom po obrtaju")
    except ValueError:
        print("mora biti broj")

def pretrazi_motor_vreme():
    """ Funkcija koja  pretrazuje motore po vremenu rada"""
    lista = ucitavanje_motora()
    pretraga = []
    brojac = 0
    tabela = []
    rec = input("unesite vreme rada: ")
    try:
        for l in lista:
            if l.vreme_rada >= int(rec):
                pretraga.append(Motor(l.oznaka, l.opis, l.duzina, l.sirina, l.visina, l.robot, l.tezina, l.vreme_rada, l.obrtaja_u_minuti, l.potrosnja_po_obrtaju, l.potrosnja))

        for u in pretraga:
            oznaka = u.oznaka
            opis = u.opis
            duzina = u.duzina
            sirina = u.sirina
            visina = u.visina
            tezina = u.tezina
            vreme_rada = u.vreme_rada
            obrtaja_u_minuti = u.obrtaja_u_minuti
            potrosnja_po_obrtaju = u.potrosnja_po_obrtaju
            potrosnja = u.potrosnja
            tabela.append([oznaka,opis,duzina,sirina,visina,tezina,vreme_rada,obrtaja_u_minuti,potrosnja_po_obrtaju,potrosnja])
            brojac +=1
        if brojac > 0:
            stampaj(tabela)      
        elif brojac ==0:
            print("ne postoji motor sa tolkim vremenom rada")
    except ValueError:
        print("mora biti broj")
def lista_delova():
    """funkcija koja trazi oznaku robota, prolazi kroz liste delova,
    i dodaje delove koji imaju oznaku robota"""
    motori = ucitavanje_motora()
    okviri = Podaci.Rad_sa_okvirom.ucitavanje_okvira()
    senzori = Podaci.Rad_sa_senzorom.ucitavanje_senzora()
    lista1 =[]
    lista2 =[]
    lista3 =[]
    unos = input("unesite oznaku robota: ")
    for o in okviri:
        if unos == o.robot.oznaka:
            lista1.append(o)
    for s in senzori:
        if unos == s.robot.oznaka:
            lista2.append(s)
    for m in motori:
        if unos == m.robot.oznaka:
            lista3.append(m)
    return lista1, lista2, lista3

def prikaz_delova():
    """funkcija koja pravi tabele delova"""
    br1, br2, br3= lista_delova()
    print("Okviri:")
    stampaj(vrednosti_okvira(br1),[['Oznaka Okvira', 'opis', 'duzina', 'sirina', 'visina', 'tezina', 'tip materijala']] )
    print("Senzori:")
    stampaj(vrednosti_senzora(br2),[['Oznaka Senzora', 'Opis', 'duzina', 'sirina', 'visina', 'tip', 'merna jedinica', 'izmerena vrednost', 'potrosnja po merenju', 'broj merenja']])
    print("Motori:")
    stampaj(vrednosti_moto(br3),[['oznaka', 'opis', 'duzina', 'sirina', 'visina', 'tezina', 'vreme rada', 'obrtaja u minuti', 'potrosnja po obrtaju', 'potrosnja']])
def vrednosti_okvira(lista1):
    """funkcija koja uzima vrednosti iz liste okvira"""
    tabela1 = []
    for u in lista1:
        oznakaO = u.oznaka
        opisO = u.opis
        duzinaO = u.duzina
        sirinaO = u.sirina
        visinaO = u.visina
        tezinaO = u.tezina
        tip_materijala = u.tip_materijala
        tabela1.append([oznakaO,opisO,duzinaO, sirinaO, visinaO, tezinaO, tip_materijala])
    return tabela1
def vrednosti_senzora(lista2):
    """funkcija koja uzima vrednosti iz liste senzora"""
    tabela2 = []   
    for l in lista2:
        oznakaS = l.oznaka
        opisS = l.opis
        duzinaS = l.duzina
        sirinaS = l.sirina
        visinaS = l.visina
        tip = l.tip
        merna_jedinica = l.merna_jedinica
        izmerena_vrednost = l.izmerena_vrednost
        potrosnja_po_merenju = l.potrosnja_po_merenju
        broj_merenja = l.broj_merenja
        tabela2.append([oznakaS,opisS,duzinaS,sirinaS,visinaS,tip,merna_jedinica,izmerena_vrednost,potrosnja_po_merenju,broj_merenja])
    return tabela2

def vrednosti_moto(lista3):
    """funkcija koja uzima vrednosti iz liste motora"""
    tabela3 = []
    for k in lista3:
        oznaka =k.oznaka
        opis = k.opis
        duzina = k.duzina
        sirina = k.sirina
        visina = k.visina
        tezina = k.tezina
        vreme_rada = k.vreme_rada
        obrtaja_u_minuti = k.obrtaja_u_minuti
        potrosnja_po_obrtaju = k.potrosnja_po_obrtaju
        potrosnja = k.potrosnja
        tabela3.append([oznaka,opis,duzina,sirina,visina,tezina,vreme_rada,obrtaja_u_minuti,potrosnja_po_obrtaju,potrosnja])
    return tabela3

def tezina_delova():
    """funkcija koja prima liste i racuna ukupnu tezinu delova motora i okvira"""
    sum1 = 0
    sum2 = 0
    br1, br2, br3 = lista_delova()
    for o in br1:
        sum1 += o.tezina
    for m in br3:
        sum2 += m.tezina
    suma = sum1 + sum2
    print("tezina delova je: ", suma)

if __name__ == '__main__':
    pass
