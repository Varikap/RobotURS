import os
from tabulate import tabulate
import Podaci.Rad_sa_podacima
from Entiteti.Model import Senzor
from Entiteti.Model import Robot
from Entiteti.Model import ElektricniDeo

senzorFajl = os.path.join(os.path.dirname(__file__), "fajlovi", "senzori.txt")

def ucitavanje_senzora():
    """ Funkcija koja ucitava sve senzore, pravi objekte i vraca listu istih """
    senzori = []
    with open(senzorFajl, "r") as f:
        for l in f.readlines():
            polja = l.strip().split("|")
            oznaka = polja[0]
            opis = polja[1]
            duzina = float(polja[2])
            sirina = float(polja[3])
            visina = float(polja[4])
            tip = polja[5]
            merna_jedinica = polja[6]
            izmerena_vrednost = float(polja[7])
            potrosnja_po_merenju = float(polja[8])
            broj_merenja = float(polja[9])
            oznaka_robota = polja[10]
            robot = deo_za_robota(Podaci.Rad_sa_podacima.ucitavanje_robota(), oznaka_robota)
            senzori.append(Senzor(oznaka, opis, duzina, sirina, visina, robot, tip, merna_jedinica, izmerena_vrednost, potrosnja_po_merenju, broj_merenja))
    return senzori

def deo_za_robota(lista, oznaka):
    """ Funkcija koja ide kroz listu robota i
     proverava da li je njegova oznaka jednaka sa oznakom robota koja pripada senzoru """
    for l in lista:
        if oznaka == l.oznaka:
            return l

def vrednosti_senzora(senzori):
    """ Funkcija koja uzima vrednosti senzora i vraca ih u listi da bi ih pripremili za stampanje """
    tabela = []
    for u in senzori:
        oznaka = u.oznaka
        opis = u.opis
        duzina = u.duzina
        sirina = u.sirina
        visina = u.visina
        tip = u.tip
        merna_jedinica = u.merna_jedinica
        izmerena_vrednost = u.izmerena_vrednost
        potrosnja_po_merenju = u.potrosnja_po_merenju
        broj_merenja = u.broj_merenja
        tabela.append([oznaka,opis,duzina, sirina, visina, tip, merna_jedinica, izmerena_vrednost, potrosnja_po_merenju, broj_merenja])
    return tabela
        
def stampaj(tabela, headers = [['Oznaka', 'Opis', 'duzina', 'sirina', 'visina', 'tip', 'merna jedinica', 'izmerena vrednost', 'potrosnja po merenju', 'broj merenja']]):
    """funkcija koja stampa prosledjene vrednosti"""
    print(tabulate(headers + tabela, headers='firstrow', tablefmt='grid'))
        
def prikaz_senzora():
    """ Funkcija koja  poziva stampanje senzora"""
    stampaj(vrednosti_senzora(ucitavanje_senzora()))
    
def lista_za_upis_senzora():
    """funkcija koja proverava da li senzor sa datom oznakom vec postoji,
    pravi novoi senzor sa unetim podacima i prosledjuje listu za upis senzora"""
    oznake = ucitavanje_senzora()
    oznake_robota = Podaci.Rad_sa_podacima.ucitavanje_robota()
    senzori = []
    unos1 = input("Unesite oznaku senzora: ")
    for o in oznake:
        if unos1 == o.oznaka:
            print("senzor sa datom oznakom vec postoji")
            break
    else:
        unos2 = input("Unesite opis senzora: ")
        unos3 = input("Unesite duzinu senzora: ")
        unos4 = input("Unesite sirinu senzora: ")
        unos5 = input("Unesite visinu senzora: ")
        unos6 = input("Unesite tip senzora: ")
        unos7 = input("Unesite mernu jedinicu senzora: ")
        unos8 = input("Unesite potrosnju po merenju senzora: ")
        unos9 = input("Unesite broj merenja senzora: ")
        unos10 = input("Unesite oznaku robota kojem okvir ide: ")
        izmerena_vrednost =""
        try:
            for l in oznake_robota:
                if unos10 == l.oznaka:
                    oznaka = unos1
                    opis = unos2
                    duzina = float(unos3)
                    sirina = float(unos4)
                    visina = float(unos5)
                    tip = unos6
                    merna_jedinica = unos7
                    potrosnja_po_merenju = float(unos8)
                    broj_merenja = float(unos9)
                    robot = unos10
                    izmerena_vrednost = ElektricniDeo.elektricna_potrosnja(izmerena_vrednost, potrosnja_po_merenju, broj_merenja)
                    senzori.append(Senzor(oznaka, opis, duzina, sirina, visina, robot, tip, merna_jedinica, izmerena_vrednost, potrosnja_po_merenju, broj_merenja))
                    break
            else:
                print("nema robota sa tom oznakom!")
        except ValueError:
            print("niste uneli validne vrednosti!")
    return senzori

def upis_senzora(senzori):
    """funkcija koja upisuje novi red u fajlu senzori"""
    with open(senzorFajl,"a") as f:
        for r in senzori:
            f.write("{}|{}|{}|{}|{}|{}|{}|{}|{}|{}|{}\n".format(r.oznaka,r.opis,r.duzina,r.sirina,r.visina,r.tip,r.merna_jedinica,r.izmerena_vrednost,r.potrosnja_po_merenju,r.broj_merenja,r.robot))
            
def upis_izmene_senzora(senzori):
    """funkcija koja upisuje prosledjenu listu u fajl"""
    with open(senzorFajl,"w") as f:
        for r in senzori:
            f.write("{}|{}|{}|{}|{}|{}|{}|{}|{}|{}|{}\n".format(r.oznaka,r.opis,r.duzina,r.sirina,r.visina,r.tip,r.merna_jedinica,r.izmerena_vrednost,r.potrosnja_po_merenju,r.broj_merenja,r.robot.oznaka))
            
def izmena_senzora(lista):
    """funkcija koja daje opciju izmene odredjenih atributa senzora
    i menja te atribute u listi"""
    oznake_robota = Podaci.Rad_sa_podacima.ucitavanje_robota()
    unos1 = input("Unesite oznaku senzora: ")
    brojac = 0
    for d in lista:
        if d.oznaka == unos1:
            brojac +=1
            unos_odg="0"
            while unos_odg not in ["1","2",'3','4','5','6','7','8','9']:
                print("koju stvar zelite da izmenite? "
                      "\n1-opis"
                      "\n2-duzina"
                      "\n3-sirina"
                      "\n4-visina"
                      "\n5-tipa"
                      "\n6-potrosnja po merenju"
                      "\n7-broj merenja"
                      "\n8-kojem robotu pripada"
                      "\n9-sve")
                unos_odg=input("unesite broj za odgovarajucu opciju: ")
            
            if unos_odg =="1":
                unos2 = input("unesite zeljeni opis: ")
                d.opis = unos2
                return lista
                break
            elif unos_odg == "2":
                try:
                    unos3 = input("unesite novu duzinu: ")
                    d.duzina = float(unos3)
                except ValueError:
                    print("niste uneli broj")
            elif unos_odg == "3":
                unos4 = input("unesite novu sirinu: ")
                try:
                    d.sirina = float(unos4)
                except ValueError:
                    print("niste uneli broj")
            elif unos_odg == "4":
                unos5 = input("unesite novu visinu: ")
                try:
                    d.visina = float(unos5)
                except ValueError:
                    print("niste uneli broj")
            elif unos_odg =="5":
                unos6 = input("unesite tip: ")
                d.tip = unos6
                return list
            elif unos_odg == "6":
                unos7 = input("unesite potrosnju po merenju: ")
                try:
                    d.potrosnja_po_merenju = float(unos7)
                    d.izmerena_vrednost = ElektricniDeo.elektricna_potrosnja(d.izmerena_vrednost, d.potrosnja_po_merenju, d.broj_merenja)
                except ValueError:
                    print("niste uneli broj")
            elif unos_odg == "7":
                unos8 = input("unesite broj merenja: ")
                try:
                    d.broj_merenja = float(unos8)
                    d.izmerena_vrednost = ElektricniDeo.elektricna_potrosnja(d.izmerena_vrednost, d.potrosnja_po_merenju, d.broj_merenja)
                except ValueError:
                    print("niste uneli broj")
            elif unos_odg == "8":
                unos9 = input("unesite oznaku robota kojem dodeljujete: ")
                for k in oznake_robota:
                    if unos9 == k.oznaka:
                        d.robot = k
                return lista
                break
            elif unos_odg == "9":
                unos10 = input("unesite zejeni opis: ")
                unos11 = input("unesite zejenu duzinu: ")
                unos12 = input("unesite zejenu sirinu: ")
                unos13 = input("unesite zejenu visinu: ")
                unos14 = input("unesite tip: ")
                unos15 = input("unesite potrosnju po merenju: ")
                unos16 = input("unesite broj merenja: ")
                unos17 = input("unesite robota kojem pripada: ")
                try:
                    for k in oznake_robota:
                        if unos17 == k.oznaka:
                            d.robot = k
                            d.opis = unos10
                            d.duzina = float(unos11)
                            d.sirina = float(unos12)
                            d.visina = float(unos13)
                            d.tip = unos14
                            d.potrosnja_po_merenju = float(unos15)
                            d.broj_merenja = float(unos16)
                            d.izmerena_vrednost = ElektricniDeo.elektricna_potrosnja(d.izmerena_vrednost, d.potrosnja_po_merenju, d.broj_merenja)
                except ValueError:
                    print("niste uneli broj")
    if brojac ==0:
        print("nema senzora sa tom oznakom")
        return lista
    return lista
    
def stampaj_senzor(tabela, headers = [['Oznaka', 'Opis', 'duzina', 'sirina', 'visina', 'tip', 'merna jedinica', 'izmerena vrednost', 'potrosnja po merenju', 'broj merenja']]):
    """funkcija za stampanje tabele senzora"""
    print(tabulate(headers + tabela, headers='firstrow', tablefmt='grid'))
    
def pretrazi_senzor_oznaka():
    """funkcija za pretrazivanje senzora po oznaci,
    proverava da li je uneta rec u atributu oznaka,
     ako jeste dodaje ga u tabelu za stampanje"""
    lista = ucitavanje_senzora()
    pretraga = []
    brojac = 0
    rec = input("unesite oznaku: ")
    for l in lista:
        if rec.lower() in l.oznaka.lower():
            pretraga.append(Senzor(l.oznaka, l.opis, l.duzina, l.sirina, l.visina, l.robot, l.tip, l.merna_jedinica, l.izmerena_vrednost, l.potrosnja_po_merenju, l.broj_merenja))
            tabela = []
            for u in pretraga:
                oznaka = u.oznaka
                opis = u.opis
                duzina = u.duzina
                sirina = u.sirina
                visina = u.visina
                tip = u.tip
                merna_jedinica = u.merna_jedinica
                izmerena_vrednost = u.izmerena_vrednost
                potrosnja_po_merenju = u.potrosnja_po_merenju
                broj_merenja = u.broj_merenja
                tabela.append([oznaka,opis,duzina,sirina,visina,tip,merna_jedinica,izmerena_vrednost,potrosnja_po_merenju,broj_merenja])
    stampaj_senzor(tabela)
    brojac +=1
                
    if brojac ==0:
        print("ne postoji senzor sa tom oznakom")

def pretrazi_senzor_opis():
    """funkcija za pretrazivanje senzora po opisu,
    proverava da li je uneta rec u atributu opis,
     ako jeste dodaje ga u tabelu za stampanje"""
    lista = ucitavanje_senzora()
    pretraga = []
    brojac = 0
    rec = input("unesite opis: ")
    for l in lista:
        if rec.lower() in l.opis.lower():
            pretraga.append(Senzor(l.oznaka, l.opis, l.duzina, l.sirina, l.visina, l.robot, l.tip, l.merna_jedinica, l.izmerena_vrednost, l.potrosnja_po_merenju, l.broj_merenja))
            tabela = []
            for u in pretraga:
                oznaka = u.oznaka
                opis = u.opis
                duzina = u.duzina
                sirina = u.sirina
                visina = u.visina
                tip = u.tip
                merna_jedinica = u.merna_jedinica
                izmerena_vrednost = u.izmerena_vrednost
                potrosnja_po_merenju = u.potrosnja_po_merenju
                broj_merenja = u.broj_merenja
                tabela.append([oznaka,opis,duzina,sirina,visina,tip,merna_jedinica,izmerena_vrednost,potrosnja_po_merenju,broj_merenja])
    stampaj_senzor(tabela)
    brojac +=1
                
    if brojac ==0:
        print("ne postoji senzor sa tim opisom")

def pretrazi_senzor_duzina():
    """funkcija za pretrazivanje senzora po duzini,
    proverava da li je uneta rec veca ili jednaka od atributa duzina,
     ako jeste dodaje ga u tabelu za stampanje"""
    lista = ucitavanje_senzora()
    pretraga = []
    brojac = 0
    tabela = []
    rec = input("unesite duzinu: ")
    try:
        for l in lista:
            if l.duzina >= float(rec):
                pretraga.append(Senzor(l.oznaka, l.opis, l.duzina, l.sirina, l.visina, l.robot, l.tip, l.merna_jedinica, l.izmerena_vrednost, l.potrosnja_po_merenju, l.broj_merenja))

        for u in pretraga:
            oznaka = u.oznaka
            opis = u.opis
            duzina = u.duzina
            sirina = u.sirina
            visina = u.visina
            tip = u.tip
            merna_jedinica = u.merna_jedinica
            izmerena_vrednost = u.izmerena_vrednost
            potrosnja_po_merenju = u.potrosnja_po_merenju
            broj_merenja = u.broj_merenja
            tabela.append([oznaka,opis,duzina,sirina,visina,tip,merna_jedinica,izmerena_vrednost,potrosnja_po_merenju,broj_merenja])
            brojac +=1
        if brojac > 0:
            stampaj_senzor(tabela)      
        elif brojac ==0:
            print("ne postoji senzor sa tom duzinom")
    except ValueError:
        print("mora biti broj")
    
def pretrazi_senzor_sirina():
    """funkcija za pretrazivanje senzora po sirini,
    proverava da li je uneta rec veca ili jednaka od atributa sirina,
     ako jeste dodaje ga u tabelu za stampanje"""
    lista = ucitavanje_senzora()
    pretraga = []
    brojac = 0
    tabela = []
    rec = input("unesite sirinu: ")
    try:
        for l in lista:
            if l.sirina >= float(rec):
                pretraga.append(Senzor(l.oznaka, l.opis, l.duzina, l.sirina, l.visina, l.robot, l.tip, l.merna_jedinica, l.izmerena_vrednost, l.potrosnja_po_merenju, l.broj_merenja))

        for u in pretraga:
            oznaka = u.oznaka
            opis = u.opis
            duzina = u.duzina
            sirina = u.sirina
            visina = u.visina
            tip = u.tip
            merna_jedinica = u.merna_jedinica
            izmerena_vrednost = u.izmerena_vrednost
            potrosnja_po_merenju = u.potrosnja_po_merenju
            broj_merenja = u.broj_merenja
            tabela.append([oznaka,opis,duzina,sirina,visina,tip,merna_jedinica,izmerena_vrednost,potrosnja_po_merenju,broj_merenja])
            brojac +=1
        if brojac > 0:
            stampaj_senzor(tabela)      
        elif brojac ==0:
            print("ne postoji senzor sa tom sirinom")
    except ValueError:
        print("mora biti broj")

def pretrazi_senzor_visina():
    """funkcija za pretrazivanje senzora po visini,
    proverava da li je uneta rec veca ili jednaka od atributa visina,
     ako jeste dodaje ga u tabelu za stampanje"""
    lista = ucitavanje_senzora()
    pretraga = []
    brojac = 0
    tabela = []
    rec = input("unesite visinu: ")
    try:
        for l in lista:
            if l.visina >= float(rec):
                pretraga.append(Senzor(l.oznaka, l.opis, l.duzina, l.sirina, l.visina, l.robot, l.tip, l.merna_jedinica, l.izmerena_vrednost, l.potrosnja_po_merenju, l.broj_merenja))

        for u in pretraga:
            oznaka = u.oznaka
            opis = u.opis
            duzina = u.duzina
            sirina = u.sirina
            visina = u.visina
            tip = u.tip
            merna_jedinica = u.merna_jedinica
            izmerena_vrednost = u.izmerena_vrednost
            potrosnja_po_merenju = u.potrosnja_po_merenju
            broj_merenja = u.broj_merenja
            tabela.append([oznaka,opis,duzina,sirina,visina,tip,merna_jedinica,izmerena_vrednost,potrosnja_po_merenju,broj_merenja])
            brojac +=1
        if brojac > 0:
            stampaj_senzor(tabela)      
        elif brojac ==0:
            print("ne postoji senzor sa tom visinom")
    except ValueError:
        print("mora biti broj")

def pretrazi_senzor_izmerena():
    """funkcija za pretrazivanje senzora po izmerenoj vrednosti,
    proverava da li je uneta rec veca ili jednaka od atributa izmerena vrednost,
     ako jeste dodaje ga u tabelu za stampanje"""
    lista = ucitavanje_senzora()
    pretraga = []
    brojac = 0
    tabela = []
    rec = input("unesite izmerenu vrednost: ")
    try:
        for l in lista:
            if l.izmerena_vrednost >= float(rec):
                pretraga.append(Senzor(l.oznaka, l.opis, l.duzina, l.sirina, l.visina, l.robot, l.tip, l.merna_jedinica, l.izmerena_vrednost, l.potrosnja_po_merenju, l.broj_merenja))

        for u in pretraga:
            oznaka = u.oznaka
            opis = u.opis
            duzina = u.duzina
            sirina = u.sirina
            visina = u.visina
            tip = u.tip
            merna_jedinica = u.merna_jedinica
            izmerena_vrednost = u.izmerena_vrednost
            potrosnja_po_merenju = u.potrosnja_po_merenju
            broj_merenja = u.broj_merenja
            tabela.append([oznaka,opis,duzina,sirina,visina,tip,merna_jedinica,izmerena_vrednost,potrosnja_po_merenju,broj_merenja])
            brojac +=1
        if brojac > 0:
            stampaj_senzor(tabela)      
        elif brojac ==0:
            print("ne postoji senzor sa tom potrosnjom")
    except ValueError:
        print("mora biti broj")

def pretrazi_senzor_potrosnja():
    """funkcija za pretrazivanje senzora po potrosnji,
    proverava da li je uneta rec veca ili jednaka od atributa potrosnja po merenju,
     ako jeste dodaje ga u tabelu za stampanje"""
    lista = ucitavanje_senzora()
    pretraga = []
    brojac = 0
    tabela = []
    rec = input("unesite potrosnju po merenju: ")
    try:
        for l in lista:
            if l.potrosnja_po_merenju >= float(rec):
                pretraga.append(Senzor(l.oznaka, l.opis, l.duzina, l.sirina, l.visina, l.robot, l.tip, l.merna_jedinica, l.izmerena_vrednost, l.potrosnja_po_merenju, l.broj_merenja))

        for u in pretraga:
            oznaka = u.oznaka
            opis = u.opis
            duzina = u.duzina
            sirina = u.sirina
            visina = u.visina
            tip = u.tip
            merna_jedinica = u.merna_jedinica
            izmerena_vrednost = u.izmerena_vrednost
            potrosnja_po_merenju = u.potrosnja_po_merenju
            broj_merenja = u.broj_merenja
            tabela.append([oznaka,opis,duzina,sirina,visina,tip,merna_jedinica,izmerena_vrednost,potrosnja_po_merenju,broj_merenja])
            brojac +=1
        if brojac > 0:
            stampaj_senzor(tabela)      
        elif brojac ==0:
            print("ne postoji senzor sa tom potrosnjom po merenju")
    except ValueError:
        print("mora biti broj")

def pretrazi_senzor_broj():
    """funkcija za pretrazivanje senzora po broju merenja,
    proverava da li je uneta rec veca ili jednaka od atributa broj merenja,
     ako jeste dodaje ga u tabelu za stampanje"""
    lista = ucitavanje_senzora()
    pretraga = []
    brojac = 0
    tabela = []
    rec = input("unesite broj merenja: ")
    try:
        for l in lista:
            if l.broj_merenja >= float(rec):
                pretraga.append(Senzor(l.oznaka, l.opis, l.duzina, l.sirina, l.visina, l.robot, l.tip, l.merna_jedinica, l.izmerena_vrednost, l.potrosnja_po_merenju, l.broj_merenja))

        for u in pretraga:
            oznaka = u.oznaka
            opis = u.opis
            duzina = u.duzina
            sirina = u.sirina
            visina = u.visina
            tip = u.tip
            merna_jedinica = u.merna_jedinica
            izmerena_vrednost = u.izmerena_vrednost
            potrosnja_po_merenju = u.potrosnja_po_merenju
            broj_merenja = u.broj_merenja
            tabela.append([oznaka,opis,duzina,sirina,visina,tip,merna_jedinica,izmerena_vrednost,potrosnja_po_merenju,broj_merenja])
            brojac +=1
        if brojac > 0:
            stampaj_senzor(tabela)      
        elif brojac ==0:
            print("ne postoji senzor sa tim brojem merenja")
    except ValueError:
        print("mora biti broj")

def pretrazi_senzor_jedinica():
    """funkcija za pretrazivanje senzora po mernoj jedinici,
    proverava da li je uneta rec u atributu merna jedinica,
     ako jeste dodaje ga u tabelu za stampanje"""
    lista = ucitavanje_senzora()
    pretraga = []
    brojac = 0
    rec = input("unesite mernu jedinicu: ")
    for l in lista:
        if rec.lower() in l.merna_jedinica.lower():
            pretraga.append(Senzor(l.oznaka, l.opis, l.duzina, l.sirina, l.visina, l.robot, l.tip, l.merna_jedinica, l.izmerena_vrednost, l.potrosnja_po_merenju, l.broj_merenja))
            tabela = []
            for u in pretraga:
                oznaka = u.oznaka
                opis = u.opis
                duzina = u.duzina
                sirina = u.sirina
                visina = u.visina
                tip = u.tip
                merna_jedinica = u.merna_jedinica
                izmerena_vrednost = u.izmerena_vrednost
                potrosnja_po_merenju = u.potrosnja_po_merenju
                broj_merenja = u.broj_merenja
                tabela.append([oznaka,opis,duzina,sirina,visina,tip,merna_jedinica,izmerena_vrednost,potrosnja_po_merenju,broj_merenja])
    stampaj_senzor(tabela)
    brojac +=1
                
    if brojac ==0:
        print("ne postoji senzor sa tom mernom jedinicom")
        
def vrednost_recnik_senzora(senzori):
    """funkcija za uzimanje vrednosti iz recnika senzora i priprema ih za stampanje"""
    tabela = []
    for u in senzori:
        oznaka = u["oznaka"]
        opis = u["opis"]
        duzina = u["duzina"]
        sirina = u["sirina"]
        visina = u["visina"]
        tip = u["tip"]
        merna_jedinica = u["merna_jedinica"]
        izmerena_vrednost = u["izmerena_vrednost"]
        potrosnja_po_merenju = u["potrosnja_po_merenju"]
        broj_merenja = u["broj_merenja"]
        tabela.append([oznaka,opis,duzina,sirina,visina,tip,merna_jedinica,izmerena_vrednost,potrosnja_po_merenju,broj_merenja])
    return tabela

def ucitavanje_senzora_recnik():
    """funkcija koja ucitava fajl sa senzorima i pravi recnik"""
    senzori = []
    with open(senzorFajl, "r") as f:
        for l in f.readlines():
            o ={'oznaka' : l.split('|')[0],\
                'opis' : l.split('|')[1],\
                'duzina' : l.split('|')[2],\
                'sirina' : l.split('|')[3],\
                'visina' : l.split('|')[4],\
                'tip' : l.split('|')[5],\
                'merna_jedinica' : l.split('|')[6],\
                'izmerena_vrednost' : l.split('|')[7],\
                'potrosnja_po_merenju' : l.split('|')[8],\
                'broj_merenja' : l.split('|')[9],\
                }

            senzori.append(o)
    return senzori

def sortiranje(vrednost, lista):
    """funkcija za sortiranje"""
    return sorted(lista, key=lambda elem: "%s" % (elem[vrednost]))
def prikaz_sortiranje_jedinica():
    """funkcija koja koristi funkciju sortiranje po mernoj jedinici"""
    stampaj(vrednost_recnik_senzora(sortiranje("merna_jedinica",ucitavanje_senzora_recnik())))
    
def prikaz_sortiranje_broj():
    """funkcija koja koristi funkciju sortiranje po broju merenja"""
    stampaj(vrednost_recnik_senzora(sortiranje("broj_merenja",ucitavanje_senzora_recnik())))

def stampaj_robota(tabela, headers = [['Oznaka', 'Opis', 'proizvodjac']]):
    """funkcija za stampanje sa headersima za robota"""
    print(tabulate(headers + tabela, headers='firstrow', tablefmt='grid'))

def prikaz_senzora_za_robota():
    """funkcija koja trazi unos oznake senzora i ukoliko je ta oznaka jednaka oznaci nekog robota
    poziva se funkcija za stampanje robota"""
    roboti = Podaci.Rad_sa_podacima.ucitavanje_robota()
    unos = input("unesite oznaku senzora: ")
    rob = []
    brojac = 0
    with open(senzorFajl, "r") as f:
        for l in f.readlines():
            polja = l.strip().split("|")
            oznaka = polja[0]
            oznaka_robota = polja[10]
            if unos == oznaka:
                for r in roboti:
                    if r.oznaka == oznaka_robota:
                        rob.append(Robot(r.oznaka,r.opis,r.proizvodjac))
                        stampaj_robota(vrednosti_robota(rob))
                        brojac +=1
    if brojac == 0:
        print("nema senzora sa tom oznakom")

def vrednosti_robota(rob):
    """funkcija koja uzima vrednosti robota iz liste sa robotima"""
    tabela = []
    for u in rob:
        oznaka = u.oznaka
        opis = u.opis
        proizvodjac = u.proizvodjac
        tabela.append([oznaka,opis,proizvodjac])
    return tabela
    
if __name__ == '__main__':
    pass