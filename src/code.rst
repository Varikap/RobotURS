#####
Klase
#####

*********
Entiteti
*********
.. automodule:: Entiteti
   :members:


Model
======
.. automodule:: Entiteti.Model
   :members:

******
Podaci
******
.. automodule:: Podaci
   :members:

Rad sa motorom
===============
.. automodule:: Podaci.Rad_sa_motorom
   :members:

Rad sa okvirom
===============
.. automodule:: Podaci.Rad_sa_okvirom
   :members:

Rad sa podacima
================
.. automodule:: Podaci.Rad_sa_podacima
   :members:

Rad sa senzorom
================
.. automodule:: Podaci.Rad_sa_senzorom
   :members: