import Podaci.Rad_sa_podacima
import Podaci.Rad_sa_okvirom
import Podaci.Rad_sa_senzorom
import Podaci.Rad_sa_motorom


def meni():
    """funkcija za prikaz menija"""
    unos = "0"
    print(" 1-Rad sa robotom."
          "\n 2-Rad sa okvirom."
          "\n 3-Rad sa motorom."
          "\n 4-Rad sa senzorom."
          "\n 5-izlaz iz aplikacije.")
    while unos not in ["1","2","3","4","5"]:
        unos = input("Unesite broj za zeljenu opciju: ")
    if unos == "1":
        unos2="0"
        print("izabrali ste opciju Rad sa robotom"
              "\n 1-Prikaz svih robota."
              "\n 2-Dodavanje novog robota."
              "\n 3-Izmena vrednosti robota."
              "\n 4-Pretraga robota po: "
              "\n 5-Prikaz delova robota."
              "\n 6-Ukupna tezina mehanickih delova robota.")
        while unos2 not in["1","2","3","4","5","6","7"]:
            unos2=input("Unesite broj za zeljenu opciju: ")
        if unos2 == "1":
            Podaci.Rad_sa_podacima.prikaz_robota()
            meni()
        elif unos2 == "2":
            Podaci.Rad_sa_podacima.upis_robota(Podaci.Rad_sa_podacima.lista_za_upis_robota())
            meni()
        elif unos2 == "3":
            Podaci.Rad_sa_podacima.upis_izmene_robota(Podaci.Rad_sa_podacima.izmena_robota(Podaci.Rad_sa_podacima.ucitavanje_robota()))
            meni()
        elif unos2 == "4":
            print(" 1-Pretraga po oznaci"
                  "\n 2-Pretraga po opisu"
                  "\n 3-Pretraga po proizvodjacu")
            gd = "0"
            while gd not in ["1","2","3"]:
                gd = input("unesite broj za opciju: ")
                
            if gd == "1":
                Podaci.Rad_sa_podacima.pretrazi_robota_oznaka()
                meni()
            elif gd == "2":
                Podaci.Rad_sa_podacima.pretrazi_robota_opis()
                meni()
            elif gd == "3":
                Podaci.Rad_sa_podacima.pretrazi_robota_proizvodjac()
                meni()
        elif unos2 == "5":
            Podaci.Rad_sa_motorom.prikaz_delova()
            meni()
        elif unos2 == "6":
            Podaci.Rad_sa_motorom.tezina_delova()
            meni()
    elif unos == "2":
        unos2="0"
        print("izabrali ste opciju Rad sa okvirom: "
              "\n 1-Prikaz svih okvira."
              "\n 2-Dodavanje novog okvira."
              "\n 3-Izmena vrednosti okvira."
              "\n 4-Pretraga okvira po: "
              "\n 5-Okviri sortirani po tezini: "
              "\n 6-Prikaz robota kojem okvir pripada: ")
        while unos2 not in["1","2","3","4","5","6"]:
            unos2=input("Unesite broj za zeljenu opciju: ")
        if unos2 == "1":
            Podaci.Rad_sa_okvirom.prikaz_okvira()
            meni()
        elif unos2 == "2":
            Podaci.Rad_sa_okvirom.upis_okvira(Podaci.Rad_sa_okvirom.lista_za_upis_okvira())
            meni()
        elif unos2 == "3":
            Podaci.Rad_sa_okvirom.upis_izmene_okvira(Podaci.Rad_sa_okvirom.izmena_okvira(Podaci.Rad_sa_okvirom.ucitavanje_okvira()))
            meni()
        elif unos2 == "4":
            print(" 1-Pretraga po oznaci"
                  "\n 2-Pretraga po opisu"
                  "\n 3-Pretraga po duzini"
                  "\n 4-Pretraga po sirini"
                  "\n 5-Pretraga po visini"
                  "\n 6-Pretraga po tezini"
                  "\n 7-Pretraga po tipu materijala")
            gl = "0"
            while gl not in ["1","2","3","4","5","6","7"]:
                gl = input("unesite broj za opciju: ")
                
            if gl == "1":
                Podaci.Rad_sa_okvirom.pretrazi_okvir_oznaka()
                meni()
            elif gl == "2":
                Podaci.Rad_sa_okvirom.pretrazi_okvir_opis()
                meni()
            elif gl == "3":
                Podaci.Rad_sa_okvirom.pretrazi_okvir_duzina()
                meni()
            elif gl == "4":
                Podaci.Rad_sa_okvirom.pretrazi_okvir_sirina()
                meni()
            elif gl == "5":
                Podaci.Rad_sa_okvirom.pretrazi_okvir_visina()
                meni()
            elif gl == "6":
                Podaci.Rad_sa_okvirom.pretrazi_okvir_tezina()
                meni()
            elif gl == "7":
                Podaci.Rad_sa_okvirom.pretrazi_okvir_tip()
                meni()
        elif unos2 == "5":
            Podaci.Rad_sa_okvirom.prikaz_tezina_sortirano()
            meni()
        elif unos2 == "6":
            Podaci.Rad_sa_okvirom.prikaz_okvira_za_robota()
            meni()
        
    elif unos == "3":
        unos2 ="0"
        print("izabrali ste opciju Rad sa motorom"
              "\n 1-Prikaz svih motora."
              "\n 2-Dodavanje novog motora."
              "\n 3-Izmena vrednosti motora."
              "\n 4-Pretraga motora po: "
              "\n 5-sortiranje po tezini: "
              "\n 6-sortiranje po potrosnji: "
              "\n 7-prikaz robota za dati motor: ")
        while unos2 not in["1","2","3","4","5","6","7"]:
            unos2=input("Unesite broj za zeljenu opciju: ")
        if unos2 == "1":
            Podaci.Rad_sa_motorom.prikaz_motora()
            meni()
        elif unos2 == "2":
            Podaci.Rad_sa_motorom.upis_motora(Podaci.Rad_sa_motorom.lista_za_upis_motora())
            meni()
        elif unos2 == "3":
            Podaci.Rad_sa_motorom.upis_izmene_motora(Podaci.Rad_sa_motorom.izmena_motora(Podaci.Rad_sa_motorom.ucitavanje_motora()))
            meni()
        elif unos2 == "4":
            print(" 1-Pretraga po oznaci"
                "\n 2-Pretraga po opisu"
                "\n 3-Pretraga po duzini"
                "\n 4-Pretraga po sirini"
                "\n 5-Pretraga po visini"
                "\n 6-Pretraga po tezini"
                "\n 7-Pretraga po vremenu rada"
                "\n 8-Pretraga obrtajima u minuti"
                "\n 9-Pretraga po potrosnji po obrtaju")
            gl = "0"
            while gl not in ["1","2","3","4","5","6","7","8","9"]:
                gl = input("unesite broj za opciju: ")
                
            if gl == "1":
                Podaci.Rad_sa_motorom.pretrazi_motor_oznaka()
                meni()
            elif gl == "2":
                Podaci.Rad_sa_motorom.pretrazi_motor_opis()
                meni()
            elif gl == "3":
                Podaci.Rad_sa_motorom.pretrazi_motor_duzina()
                meni()
            elif gl == "4":
                Podaci.Rad_sa_motorom.pretrazi_motor_sirina()
                meni()
            elif gl == "5":
                Podaci.Rad_sa_motorom.pretrazi_motor_visina()
                meni()
            elif gl == "6":
                Podaci.Rad_sa_motorom.pretrazi_motor_tezina()
                meni()
            elif gl == "7":
                Podaci.Rad_sa_motorom.pretrazi_motor_vreme()
                meni()
            elif gl == "8":
                Podaci.Rad_sa_motorom.pretrazi_motor_obrtaja()
                meni()
            elif gl == "9":
                Podaci.Rad_sa_motorom.pretrazi_motor_potrosnja()
                meni()
        elif unos2 == "5":
            Podaci.Rad_sa_motorom.prikaz_sortiranje_tezina()
            meni()
        elif unos2 == "6":
            Podaci.Rad_sa_motorom.prikaz_sortiranje_potrosnja()
            meni()
        elif unos2 == "7":
            Podaci.Rad_sa_motorom.prikaz_motora_za_robota()
            meni()
    elif unos == "4":
        unos2 ="0"
        print("izabrali ste opciju Rad sa senzorom"
              "\n 1-Prikaz svih senzora."
              "\n 2-Dodavanje novog senzora."
              "\n 3-Izmena vrednosti senzora."
              "\n 4-Pretraga senzora po: "
              "\n 5-Sortiranje senzora po mernoj jedinici: "
              "\n 6-Sortiranje senzora po broju merenja: "
              "\n 7-Prikaz robota za dati senzor: ")
        while unos2 not in["1","2","3","4","5","6","7"]:
            unos2=input("Unesite broj za zeljenu opciju: ")
        if unos2 == "1":
            Podaci.Rad_sa_senzorom.prikaz_senzora()
            meni()
        elif unos2 == "2":
            Podaci.Rad_sa_senzorom.upis_senzora(Podaci.Rad_sa_senzorom.lista_za_upis_senzora())
            meni()
        elif unos2 == "3":
            Podaci.Rad_sa_senzorom.upis_izmene_senzora(Podaci.Rad_sa_senzorom.izmena_senzora(Podaci.Rad_sa_senzorom.ucitavanje_senzora()))
            meni()
        elif unos2 == "4":
            print(" 1-Pretraga po oznaci"
                  "\n 2-Pretraga po opisu"
                  "\n 3-Pretraga po duzini"
                  "\n 4-Pretraga po sirini"
                  "\n 5-Pretraga po visini"
                  "\n 6-Pretraga po mernoj jedinici"
                  "\n 7-Pretraga po izmerenoj vrednosti"
                  "\n 8-Pretraga po potrosnji po merenju"
                  "\n 9-Pretraga po broju merenja")
            gl = "0"
            while gl not in ["1","2","3","4","5","6","7","8","9"]:
                gl = input("unesite broj za opciju: ")
                
            if gl == "1":
                Podaci.Rad_sa_senzorom.pretrazi_senzor_oznaka()
                meni()
            elif gl == "2":
                Podaci.Rad_sa_senzorom.pretrazi_senzor_opis()
                meni()
            elif gl == "3":
                Podaci.Rad_sa_senzorom.pretrazi_senzor_duzina()
                meni()
            elif gl == "4":
                Podaci.Rad_sa_senzorom.pretrazi_senzor_sirina()
                meni()
            elif gl == "5":
                Podaci.Rad_sa_senzorom.pretrazi_senzor_visina()
                meni()
            elif gl == "6":
                Podaci.Rad_sa_senzorom.pretrazi_senzor_jedinica()
                meni()
            elif gl == "7":
                Podaci.Rad_sa_senzorom.pretrazi_senzor_izmerena()
                meni()
            elif gl == "8":
                Podaci.Rad_sa_senzorom.pretrazi_senzor_potrosnja()
                meni()
            elif gl == "9":
                Podaci.Rad_sa_senzorom.pretrazi_senzor_broj()
                meni()
        elif unos2 == "5":
            Podaci.Rad_sa_senzorom.prikaz_sortiranje_jedinica()
            meni()
        elif unos2 == "6":
            Podaci.Rad_sa_senzorom.prikaz_sortiranje_broj()
            meni()
        elif unos2 == "7":
            Podaci.Rad_sa_senzorom.prikaz_senzora_za_robota()
            meni()
            
    elif unos == "5":
        print("izlazim iz aplikacije")
        exit()
    
if __name__ == '__main__':
    print("Dobrodosli u aplikaciju!")
    meni()
