.. RobotPr documentation master file, created by
   sphinx-quickstart on Fri Jul  7 21:39:27 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to RobotPr's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   code

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
