class Identifikacija(object):
    """klasa identifikacije"""
    def __init__(self, oznaka, opis):
        self.oznaka = oznaka
        self.opis = opis
        
    
    def __str__(self):
        return "{} {}".format(self.oznaka, self.opis)
    
    
class Robot(Identifikacija):
    """klasa robot koja nasledjuje identifikacije"""
    def __init__(self, oznaka, opis, proizvodjac):
        Identifikacija.__init__(self, oznaka, opis)
        self.proizvodjac = proizvodjac
    def __str__(self):
        return "{} {} {}".format(self.oznaka, self.opis, self.proizvodjac)
    
    
class Dimenzije(object):
    """klasa dimenzije"""
    def __init__(self, duzina, sirina, visina):
        self.duzina = duzina
        self.sirina = sirina
        self.visina = visina
    
    @property
    def duzina(self):
        return self.__duzina
        
    @duzina.setter
    def duzina(self, value):
        if value <= 0:
            raise ValueError("duzina mora biti veca od nule ")
        self.__duzina = value
        
    @property
    def sirina(self):
        return self.__sirina
        
    @sirina.setter
    def sirina(self, value):
        if value <= 0:
            raise ValueError("sirina mora biti veca od nule ")
        self.__sirina = value
    @property
    def visina(self):
        return self.__visina
        
    @visina.setter
    def visina(self, value):
        if value <= 0:
            raise ValueError("visina mora biti veca od nule ")
        self.__visina = value
    def __str__(self):
        return "{} {} {}".format(self.duzina, self.sirina, self.visina)
    
    
class Deo(Identifikacija, Dimenzije):
    """klasa deo koja nasledjuje identifikacije i dimenzije"""
    def __init__(self, oznaka, opis, duzina, sirina, visina, robot):
        Identifikacija.__init__(self, oznaka, opis)
        Dimenzije.__init__(self, duzina, sirina, visina)
        self.robot = robot
    def __str__(self):
        return "{} {} {} {} {} {}".format(self.oznaka, self.opis, self.duzina, self.sirina, self.visina, self.robot)


class MehanickiDeo(Deo):
    """klasa mehanicki deo koji nasledjuje klasu Deo"""
    def __init__(self, oznaka, opis, duzina, sirina, visina, robot, tezina):
        Deo.__init__(self, oznaka, opis, duzina, sirina, visina, robot)
        self.tezina = tezina
    @property
    def tezina(self):
        return self.__tezina
        
    @tezina.setter
    def tezina(self, value):
        if value <= 0:
            raise ValueError("tezina mora biti veca od nule ")
        self.__tezina = value   
    def __str__(self):
        return "{} {} {} {} {} {} {}".format(self.oznaka, self.opis, self.duzina, self.sirina, self.visina, self.robot, self.tezina)
    
    
class ElektricniDeo(Deo):
    """klasa elektricni deo koja nasledjuje klasu Deo"""
    def __init__(self, oznaka, opis, duzina, sirina, visina, robot):
        Deo.__init__(self, oznaka, opis, duzina, sirina, visina, robot)
    
    def elektricna_potrosnja(self, par1, par2, par3 = 1):
        el_potrosnja =float(par1 * par2 * par3)
        return el_potrosnja
    
    def __str__(self):
        return "{} {} {} {} {} {}".format(self.oznaka, self.opis, self.duzina, self.sirina, self.visina, self.robot)
        
        
class Okvir(MehanickiDeo):
    """klasa okvir koja nasledjuje klasu MehanickiDeo"""
    def __init__(self, oznaka, opis, duzina, sirina, visina, robot, tezina, tip_materijala):
        MehanickiDeo.__init__(self, oznaka, opis, duzina, sirina, visina, robot, tezina)
        self.tip_materijala = tip_materijala
    
    def __str__(self):
        return "{} {} {} {} {} {} {} {}".format(self.oznaka, self.opis, self.duzina, self.sirina, self.visina, self.robot, self.tezina, self.tip_materijala)
    
class Motor(ElektricniDeo, MehanickiDeo):
    """klasa Motor koja nasledjuje klase ElektricniDeo i MehanickiDeo"""
    def __init__(self, oznaka, opis, duzina, sirina, visina, robot, tezina, vreme_rada, obrtaja_u_minuti, potrosnja_po_obrtaju, potrosnja):
        self.oznaka = oznaka
        self.opis = opis
        self.duzina = duzina
        self.sirina = sirina
        self.visina = visina
        self.robot = robot
        self.tezina = tezina
        self.vreme_rada = vreme_rada
        self.obrtaja_u_minuti = obrtaja_u_minuti
        self.potrosnja_po_obrtaju = potrosnja_po_obrtaju
        self.potrosnja = potrosnja
        
    @property
    def vreme_rada(self):
        return self.__vreme_rada
        
    @vreme_rada.setter
    def vreme_rada(self, value):
        if value <= 0:
            raise ValueError("vreme rada mora biti veca od nule ")
        self.__vreme_rada = value
    @property
    def obrtaja_u_minuti(self):
        return self.__obrtaja_u_minuti
        
    @obrtaja_u_minuti.setter
    def obrtaja_u_minuti(self, value):
        if value <= 0:
            raise ValueError("obrtaji u minuti moraju biti veca od nule ")
        self.__obrtaja_u_minuti = value
    
    @property
    def potrosnja_po_obrtaju(self):
        return self.__potrosnja_po_obrtaju
        
    @potrosnja_po_obrtaju.setter
    def potrosnja_po_obrtaju(self, value):
        if value <= 0:
            raise ValueError("potrosnja po obrtaju mora biti veca od nule ")
        self.__potrosnja_po_obrtaju = value
        
    def __str__(self):
        return "{} {} {} {} {} {}".format(self.meh_deo, self.el_deo, self.vreme_rada, self.obrtaja_u_minuti, self.potrosnja_po_obrtaju, self.potrosnja)
    
    
class Senzor(ElektricniDeo):
    """klasa senzor koja nasledjuje klasu ElektricniDeo"""
    def __init__(self, oznaka, opis, duzina, sirina, visina, robot, tip, merna_jedinica, izmerena_vrednost, potrosnja_po_merenju, broj_merenja):
        ElektricniDeo.__init__(self, oznaka, opis, duzina, sirina, visina, robot)
        self.tip = tip
        self.merna_jedinica = merna_jedinica
        self.izmerena_vrednost = izmerena_vrednost
        self.potrosnja_po_merenju = potrosnja_po_merenju
        self.broj_merenja = broj_merenja
    
    @property
    def potrosnja_po_merenju(self):
        return self.__potrosnja_po_merenju
        
    @potrosnja_po_merenju.setter
    def potrosnja_po_merenju(self, value):
        if value <= 0:
            raise ValueError("potrosnja po merenju mora biti veca od nule ")
        self.__potrosnja_po_merenju = value
    @property
    def broj_merenja(self):
        return self.__broj_merenja
        
    @broj_merenja.setter
    def broj_merenja(self, value):
        if value <= 0:
            raise ValueError("broj merenja mora biti veca od nule ")
        self.__broj_merenja = value
        
    def __str__(self):
        return "{} {} {} {} {} {} {} {} {} {} {} {}".format(self.oznaka, self.opis, self.duzina, self.sirina, self.visina, self.robot,  self.tip, self.merna_jedinica, self.izmerena_vrednost, self.potrosnja_po_merenju, self.broj_merenja)